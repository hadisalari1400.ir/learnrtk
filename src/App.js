import { Provider } from "react-redux";
import "./App.css";

import "bootstrap/dist/css/bootstrap.min.css";
import AddTodoForm from "./components/Todos/AddTodoForm";
import TodoList from "./components/Todos/TodoList";
import Cakes from "./components/Cakes/Cake";
import { store } from "./features/store";

const App = () => {
  return (
    <Provider store={store}>
      <div className="container">
        <h1>hi Hadi !</h1>

        <Cakes />

        <div>
          <AddTodoForm />
          <TodoList />
        </div>
      </div>
    </Provider>
  );
};

export default App;
