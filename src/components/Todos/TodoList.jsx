import React, { useEffect } from "react";
import TodoItem from "./TodoItem";
import { useDispatch, useSelector } from "react-redux";
import { getAsyncTodos } from "../../features/todos/todoReducer";

const TodoList = () => {
  const { todos, error, loading } = useSelector((state) => state.todo);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAsyncTodos());
  }, []);

  return (
    <div>
      <ul className="list-group">
        {loading ? (
          <p>loading ...</p>
        ) : error ? (
          <p> {error} </p>
        ) : todos.length ? (
          todos.map((todo) => (
            <TodoItem key={todo.id} {...todo} dispatch={dispatch} />
          ))
        ) : null}
      </ul>
      <h5 className="mt-3">Total Items: {todos.length} </h5>
      <h5 className="mt-3">
        Total Complete Items:{" "}
        {todos.filter((todo) => todo.completed === true).length}
      </h5>
    </div>
  );
};

export default TodoList;
