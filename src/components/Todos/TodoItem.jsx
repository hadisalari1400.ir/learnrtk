import React from "react";
import {
  deleteAsyncTodos,
  editAsyncTodos,
} from "../../features/todos/todoReducer";

const TodoItem = ({ id, title, completed, dispatch }) => {
  return (
    <li className={`list-group-item ${completed && "list-group-item-success"}`}>
      <div className="d-flex justify-content-between">
        <span className="d-flex align-items-center">
          <input
            type="checkbox"
            className="mr-3"
            checked={completed}
            onChange={() =>
              dispatch(editAsyncTodos({ id, title, completed: !completed }))
            }
          ></input>
          {title}
        </span>
        <button
          className="btn btn-danger"
          onClick={() => dispatch(deleteAsyncTodos(id))}
        >
          Delete
        </button>
      </div>
    </li>
  );
};

export default TodoItem;
