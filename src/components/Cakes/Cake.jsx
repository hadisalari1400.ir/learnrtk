import { useDispatch, useSelector } from "react-redux";
import { decrement, increment } from "../../features/cakes/cakeReducer";

const Cakes = () => {
  const { value } = useSelector((state) => state.cake);
  const dispatch = useDispatch();

  return (
    <div>
      <h4>number of cakes : {value} </h4>
      <button onClick={() => dispatch(increment())}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>
    </div>
  );
};

export default Cakes;
