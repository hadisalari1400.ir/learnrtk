import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const getAsyncTodos = createAsyncThunk(
  "todo/getTodos",
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await axios.get("http://localhost:3001/todos");
      return data;
    } catch (err) {
      return rejectWithValue(err.message);
    }
  }
);

export const addAsyncTodos = createAsyncThunk(
  "todo/addAsyncTodos",
  async (payload, { rejectWithValue }) => {
    try {
      const { data } = await axios.post("http://localhost:3001/todos", payload);
      return data;
    } catch (err) {
      return rejectWithValue(err.message);
    }
  }
);

export const editAsyncTodos = createAsyncThunk(
  "todo/editAsyncTodos",
  async (payload, { rejectWithValue }) => {
    try {
      const { data } = await axios.put(
        `http://localhost:3001/todos/${payload.id}`,
        payload
      );
      return data;
    } catch (err) {
      return rejectWithValue(err.message);
    }
  }
);

export const deleteAsyncTodos = createAsyncThunk(
  "todo/deleteAsyncTodos",
  async (payload, { rejectWithValue }) => {
    try {
      await axios.delete(`http://localhost:3001/todos/${payload}`);
      return payload;
    } catch (err) {
      return rejectWithValue(err.message);
    }
  }
);

const todoReducer = createSlice({
  name: "todo",
  initialState: { todos: [], loading: false, error: "" },
  extraReducers: {
    // get todos
    [getAsyncTodos.fulfilled]: (state, { payload }) => {
      return { todos: payload, loading: false, error: "" };
    },
    [getAsyncTodos.pending]: () => {
      return { todos: [], loading: true, error: "" };
    },
    [getAsyncTodos.rejected]: (state, { err }) => {
      return { todos: [], loading: false, error: err };
    },

    // add todo
    [addAsyncTodos.fulfilled]: (state, { payload }) => {
      state.todos.push(payload);
    },

    // edit todo
    [editAsyncTodos.fulfilled]: (state, { payload }) => {
      const todo = state.todos.find((todo) => todo.id === payload.id);
      todo.completed = !todo.completed;
    },

    // delete todo
    [deleteAsyncTodos.fulfilled]: (state, { payload }) => {
      state.todos = state.todos.filter((todo) => todo.id !== payload);
    },
  },
});

export default todoReducer.reducer;
