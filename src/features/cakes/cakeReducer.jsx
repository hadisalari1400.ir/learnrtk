import { createSlice } from "@reduxjs/toolkit";

const cakeReducer = createSlice({
  name: "cake",
  initialState: { value: 10 },
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
  },
});

// Action creators are generated for each case reducer function
export const { increment, decrement } = cakeReducer.actions;

export default cakeReducer.reducer;
