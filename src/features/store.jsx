import { configureStore } from "@reduxjs/toolkit";
import cakeReducer from "./cakes/cakeReducer";
import todoReducer from "./todos/todoReducer";

export const store = configureStore({
  reducer: {
    todo: todoReducer,
    cake: cakeReducer,
  },
});
